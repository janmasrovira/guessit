module Picker exposing(..)

import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Array
import Array exposing (Array)
import Bootstrap.Html exposing (..)
import BootstrapAux exposing (..)

-- { id, class, classList } = homepageNamespace
type alias Entry msg = {
        event : msg
    , text : String
    , badges : List String
    , enabled : Bool
    , style : Style
    , bsize : BSize
    }

type alias Description msg =
    {
        entries : List (Entry msg)
    }

view' : Description msg -> Html msg
view' desc = div []
    [div [class "col-md-4"] []
    , div [class "btn-group-vertical col-md-4"]
        (List.map (mkButton desc) desc.entries)
    ]

view : Description msg -> Html msg
view desc = div [class "text-center"]
    [div [class "btn-group-vertical"]
        (List.map (mkButton desc) desc.entries)
    ]

mkButton : Description msg -> Entry msg -> Html msg
mkButton descript entry =
    btnB entry.style entry.bsize "" entry.enabled entry.badges
        {icon=Nothing, label=Just entry.text, tooltip=Nothing}
            entry.event
