module Collections.SaintSaens exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Saint-Saens - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Symphonies (Soustrot)", "creator": "Camille Saint-Saens", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/SaintSaens/symphony 1.ogg", "duration": "2028000"}, {"album": "Symphonies (Soustrot)", "creator": "Camille Saint-Saens", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/SaintSaens/symphony 2.ogg", "duration": "1376000"}, {"album": "Symphonies (Soustrot)", "creator": "Camille Saint-Saens", "title": "symphony 3", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/SaintSaens/symphony 3.ogg", "duration": "1376000"}, {"album": "Symphonies (Soustrot)", "creator": "Camille Saint-Saens", "title": "symphony in A major", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/SaintSaens/symphony in A major.ogg", "duration": "1585000"}]}}}
 """
