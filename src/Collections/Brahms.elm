module Collections.Brahms exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Brahms - Symphonies", playlist=Collection.unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Brahms Symphony No.1, Op.68", "creator": "Brahms", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Brahms/symphony 1.ogg", "duration": "2665000"}, {"album": "Brahms : Symphonies Nos.2 & 3", "creator": "Brahms", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Brahms/symphony 2.ogg", "duration": "2335000"}, {"album": "Brahms : Symphonies Nos.2 & 3", "creator": "Brahms", "title": "symphony 3", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Brahms/symphony 3.ogg", "duration": "2368000"}, {"album": "Symphony No.4 .Hungarian Dances", "creator": "Brahms", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Brahms/symphony 4.ogg", "duration": "2433000"}]}}}
"""
