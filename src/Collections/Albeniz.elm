module Collections.Albeniz exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Albeniz - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Iberia (Orchestra) (Lopez Cobos)", "creator": "Isaac Albeniz", "title": "Suite Iberia", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Albeniz/Suite Iberia.ogg", "duration": "4935000"}]}}}
    """
