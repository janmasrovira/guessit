module Collections.Dvorak exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Dvorak - Symphonies", playlist=Collection.unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "The Symphonies & Tone Poems", "creator": "Dvorak", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 1.ogg", "duration": "3255000"}, {"album": "The Symphonies & Tone Poems", "creator": "Dvorak", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 2.ogg", "duration": "3272000"}, {"album": "Dvorak: Symphonies Nos. 3 & 7", "creator": "Dvorak", "title": "symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 3.ogg", "duration": "2035000"}, {"album": "The Symphonies & Tone Poems", "creator": "Dvorak", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 4.ogg", "duration": "2422000"}, {"album": "The Symphonies & Tone Poems", "creator": "Dvorak", "title": "symphony 5", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 5.ogg", "duration": "2421000"}, {"album": "Dvorak: Symphony n6", "creator": "Dvorak", "title": "symphony 6", "trackNum": "2", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 6.ogg", "duration": "2582000"}, {"album": "Symphonies 7 & 8", "creator": "Dvorak", "title": "symphony 7", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 7.ogg", "duration": "2193000"}, {"album": "Symphonies 7 & 8", "creator": "Dvorak", "title": "symphony 8", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 8.ogg", "duration": "2227000"}, {"album": "Symphonie No. 9 'Aus der neuen Welt'", "creator": "Dvorak", "title": "symphony 9", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Dvorak/symphony 9.ogg", "duration": "3022000"}]}}}
"""
