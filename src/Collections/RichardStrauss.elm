module Collections.RichardStrauss exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Richard Strauss - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Also Sprach Zarathustra, Tod und Verklarung (Sinopoli)", "creator": "Richard Strauss", "title": "Also Sprach Zarathustra", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Also Sprach Zarathustra.ogg", "duration": "2232000"}, {"album": "Haydn-J. Strauss II-R. Strauss-Lear", "creator": "Richard Strauss", "title": "Der Rosenkavalier Suite", "trackNum": "9", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Der Rosenkavalier Suite.ogg", "duration": "1267000"}, {"album": "Don Quixote / Don Juan (Reiner)", "creator": "Richard Strauss", "title": "Don Quixote", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Don Quixote.ogg", "duration": "2583000"}, {"album": "Strauss: An Alpine Symphony (Charles Dutoit)", "creator": "Richard Strauss", "title": "Eine Alpensinfonie", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Eine Alpensinfonie.ogg", "duration": "3116000"}, {"duration": "742000", "trackNum": "1", "creator": "Richard Strauss", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Festive Prelude for Organ and Orchestra.ogg", "title": "Festive Prelude for Organ and Orchestra"}, {"album": "Symphonia domestica / Le bourgeois gentilhomme (Reiner)", "creator": "Richard Strauss", "title": "Le Bourgeois Gentilhomme", "trackNum": "6", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Le Bourgeois Gentilhomme.ogg", "duration": "1813000"}, {"album": "Symphonia domestica / Le bourgeois gentilhomme (Reiner)", "creator": "Richard Strauss", "title": "Symphonia Domestica", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Symphonia Domestica.ogg", "duration": "2626000"}, {"album": "Also Sprach Zarathustra, Till Eulenspiegels, Don Juan Op. 20 (Karajan)", "creator": "Richard Strauss", "title": "Till Eulenspiegels", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Till Eulenspiegels.ogg", "duration": "937000"}, {"album": "Also Sprach Zarathustra, Tod und Verklarung (Sinopoli)", "creator": "Richard Strauss", "title": "Tod und Verklarung", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Richard Strauss/Tod und Verklarung.ogg", "duration": "1701000"}]}}}
"""
