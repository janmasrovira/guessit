module Collections.Rachmaninoff exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Rachmaninoff - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Rachmaninov Orchestral Music (Previn)", "creator": "Rachmaninoff", "title": "Isle of the Dead", "trackNum": "4", "location": "/media/jan/LocalDisk/Guessit/Rachmaninoff/Isle of the Dead.ogg", "duration": "1276000"}, {"album": "Rachmaninov Orchestral Music (Previn)", "creator": "Rachmaninoff", "title": "Symphonic Dances", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Rachmaninoff/Symphonic Dances.ogg", "duration": "2100000"}, {"album": "Rachmaninoff Symphony No.2, Vocalise, Aleko", "creator": "Rachmaninoff", "title": "Symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Rachmaninoff/Symphony 2.ogg", "duration": "3563000"}, {"album": "Rachmaninov Orchestral Music (Previn)", "creator": "Rachmaninoff", "title": "Vocalise", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Rachmaninoff/Vocalise.ogg", "duration": "396000"}]}}}
 """
