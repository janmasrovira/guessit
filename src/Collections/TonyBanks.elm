module Collections.TonyBanks exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Tony Banks - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
     {"playlist": {"trackList": {"track": [{"album": "Seven: A Suite for Orchestra", "creator": "Tony Banks", "title": "Seven: A Suite for Orchestra", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Tony Banks/Seven: A Suite for Orchestra.ogg", "duration": "3457000"}]}}}
     """
