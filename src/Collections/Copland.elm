module Collections.Copland exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Copland - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Great Performances-Copland: Appalachian Spring Suite; Fanfare for the Common Man; El Salon Mexico; Danzon Cubano (Bernstein, NYPO)", "creator": "Aaron Copland", "title": "Appalachian Spring", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Copland/Appalachian Spring.ogg", "duration": "1483000"}, {"album": "Great Performances-Copland: Appalachian Spring Suite; Fanfare for the Common Man; El Salon Mexico; Danzon Cubano (Bernstein, NYPO)", "creator": "Aaron Copland", "title": "Danzon Cubano", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Copland/Danzon Cubano.ogg", "duration": "407000"}, {"album": "Great Performances-Copland: Appalachian Spring Suite; Fanfare for the Common Man; El Salon Mexico; Danzon Cubano (Bernstein, NYPO)", "creator": "Aaron Copland", "title": "El Salon Mexico", "trackNum": "9", "location": "/media/jan/LocalDisk/Guessit/Copland/El Salon Mexico.ogg", "duration": "661000"}, {"album": "Copland: Fanfare For The Common Man; Barber: Adagio", "creator": "Aaron Copland", "title": "Rodeo", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Copland/Rodeo.ogg", "duration": "1541000"}, {"album": "Copland - Organ Symphony & No.3", "creator": "Copland", "title": "Symphony 3", "trackNum": "4", "location": "/media/jan/LocalDisk/Guessit/Copland/Symphony 3.ogg", "duration": "2492000"}]}}}
"""
