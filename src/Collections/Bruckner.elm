module Collections.Bruckner exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Bruckner - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 1.ogg", "duration": "2997000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 2.ogg", "duration": "3745000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 3.ogg", "duration": "3576000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 4.ogg", "duration": "4103000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 5", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 5.ogg", "duration": "4322000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 6", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 6.ogg", "duration": "3285000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 7", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 7.ogg", "duration": "4240000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 8", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 8.ogg", "duration": "4622000"}, {"album": "The Nine Symphonies. Helgoland (Barenboim)", "creator": "Bruckner", "title": "symphony 9", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Bruckner/symphony 9.ogg", "duration": "3809000"}]}}}
 """
