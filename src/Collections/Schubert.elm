module Collections.Schubert exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Schubert - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
    {"playlist": {"trackList": {"track": [{"album": "Schubert: Complete Symphonies (Blomstedt)", "creator": "Schubert", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 1.ogg", "duration": "1901000"}, {"album": "Schubert: Complete Symphonies (Blomstedt)", "creator": "Schubert", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 2.ogg", "duration": "1742000"}, {"album": "Schubert: Complete Symphonies (Blomstedt)", "creator": "Schubert", "title": "symphony 3", "trackNum": "9", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 3.ogg", "duration": "1380000"}, {"album": "Schubert: Complete Symphonies (Blomstedt)", "creator": "Schubert", "title": "symphony 4", "trackNum": "13", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 4.ogg", "duration": "1824000"}, {"album": "Franz Schubert - Symphony No.8 & 5", "creator": "Schubert", "title": "symphony 5", "trackNum": "3", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 5.ogg", "duration": "1787000"}, {"album": "Schubert: Complete Symphonies (Blomstedt)", "creator": "Schubert", "title": "symphony 6", "trackNum": "21", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 6.ogg", "duration": "1812000"}, {"album": "Franz Schubert - Symphony No.8 & 5", "creator": "Schubert", "title": "symphony 8", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 8.ogg", "duration": "1614000"}, {"album": "CD45 Schubert Sym Nr.8,9", "creator": "Schubert", "title": "symphony 9", "trackNum": "3", "location": "/media/jan/LocalDisk/Guessit/Schubert/symphony 9.ogg", "duration": "2932000"}]}}}
"""
