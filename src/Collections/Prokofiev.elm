module Collections.Prokofiev exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Prokofiev - Symphonic", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Prokofiev: Symphonies 1 & 5, Lieutenant Kije Suite (Temirkanov)", "creator": "Prokofiev", "title": "Lieutenant Kije", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Lieutenant Kije.ogg", "duration": "1250000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 1.ogg", "duration": "882000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 2.ogg", "duration": "1990000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 3.ogg", "duration": "2180000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 4 (revised 1947)", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 4 (revised 1947).ogg", "duration": "2242000"}, {"album": "Prokofiev: Symphonies 1 & 5, Lieutenant Kije Suite (Temirkanov)", "creator": "Prokofiev", "title": "Symphony 5", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 5.ogg", "duration": "2571000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 6", "trackNum": "9", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 6.ogg", "duration": "2505000"}, {"album": "Prokofiev: The Complete Symphonies", "creator": "Prokofiev", "title": "Symphony 7", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Prokofiev/Symphony 7.ogg", "duration": "1882000"}]}}}
"""
