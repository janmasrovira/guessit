module Collections.Stravinsky exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = ballets


ballets : Collection
ballets = {
    title="Stravinsky - Ballets", playlist=unsafeDecodePlaylist s}


s : String
s = """
     {"playlist": {"trackList": {"track": [{"album": "Stravinsky: The Rite of Spring; Petrushka; The Firebird; Apollo (Rattle)", "creator": "Stravinsky", "title": "Apollo", "trackNum": "46", "location": "/media/jan/LocalDisk/Guessit/Stravinsky/Apollo.ogg", "duration": "1806000"}, {"album": "Stravinsky: The Rite of Spring; Petrushka; The Firebird; Apollo (Rattle)", "creator": "Stravinsky", "title": "Petrushka", "trackNum": "8", "location": "/media/jan/LocalDisk/Guessit/Stravinsky/Petrushka.ogg", "duration": "2104000"}, {"album": "Stravinsky: The Rite of Spring; Petrushka; The Firebird; Apollo (Rattle)", "creator": "Stravinsky", "title": "The Firebird", "trackNum": "22", "location": "/media/jan/LocalDisk/Guessit/Stravinsky/The Firebird.ogg", "duration": "2859000"}, {"album": "Anthology of Russian Symphony Music, Vol. 62: Stravinsky - Rite of Spring & Card Game", "creator": "Stravinsky", "title": "The Rite of Spring", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Stravinsky/The Rite of Spring.ogg", "duration": "2045000"}]}}}
     """
