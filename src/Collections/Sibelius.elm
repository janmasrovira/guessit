module Collections.Sibelius exposing(..)

import Collection exposing(..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Sibelius - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "CD54 Sibelius Sym Nr.1,6", "creator": "Sibelius", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 1.ogg", "duration": "2204000"}, {"album": "CD55 Sibelius Sym Nr.2,7", "creator": "Sibelius", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 2.ogg", "duration": "2676000"}, {"album": "CD53 Shostakovich Sym Nr.14;Sibelius Sym Nr.3", "creator": "Sibelius", "title": "symphony 3", "trackNum": "12", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 3.ogg", "duration": "1589000"}, {"album": "CD56 Sibelius Sym Nr.4,5", "creator": "Sibelius", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 4.ogg", "duration": "2368000"}, {"album": "CD56 Sibelius Sym Nr.4,5", "creator": "Sibelius", "title": "symphony 5", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 5.ogg", "duration": "1966000"}, {"album": "CD54 Sibelius Sym Nr.1,6", "creator": "Sibelius", "title": "symphony 6", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 6.ogg", "duration": "1598000"}, {"album": "CD55 Sibelius Sym Nr.2,7", "creator": "Sibelius", "title": "symphony 7", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Sibelius/symphony 7.ogg", "duration": "1369000"}]}}}
"""
