module Collections.Mahler exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Mahler - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "das lied von der erde", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/das lied von der erde.ogg", "duration": "4788000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 1.ogg", "duration": "3161000"}, {"album": "Symphonies (Complete) - Eliahu Inbal (1985-1992 BC 2003)", "creator": "Mahler", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 2.ogg", "duration": "5099000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 3.ogg", "duration": "5973000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 4.ogg", "duration": "3303000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 5", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 5.ogg", "duration": "4174000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 6", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 6.ogg", "duration": "4675000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 7", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 7.ogg", "duration": "4784000"}, {"album": "Symphonies (Complete) - Eliahu Inbal (1985-1992 BC 2003)", "creator": "Mahler", "title": "symphony 8", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 8.ogg", "duration": "4682000"}, {"album": "Bernstein - Mahler, The Symphonies", "creator": "Mahler", "title": "symphony 9", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mahler/symphony 9.ogg", "duration": "4788000"}]}}}
"""
