module Collections.Borodin exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Borodin - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Symphonies (Tjeknavorian)", "creator": "Borodin", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Borodin/symphony 1.ogg", "duration": "1999000"}, {"album": "Symphonies (Tjeknavorian)", "creator": "Borodin", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Borodin/symphony 2.ogg", "duration": "1602000"}, {"album": "Symphonies (Tjeknavorian)", "creator": "Borodin", "title": "symphony 3", "trackNum": "10", "location": "/media/jan/LocalDisk/Guessit/Borodin/symphony 3.ogg", "duration": "1137000"}]}}}
"""
