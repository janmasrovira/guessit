module Collections.Mendelssohn exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Mendelssohn - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "5 Symphonies | 7 Overtures", "creator": "Mendelssohn", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mendelssohn/symphony 1.ogg", "duration": "1944000"}, {"album": "5 Symphonies | 7 Overtures", "creator": "Mendelssohn", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mendelssohn/symphony 2.ogg", "duration": "4464000"}, {"album": "5 Symphonies | 7 Overtures", "creator": "Mendelssohn", "title": "symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mendelssohn/symphony 3.ogg", "duration": "2551000"}, {"album": "5 Symphonies | 7 Overtures", "creator": "Mendelssohn", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Mendelssohn/symphony 4.ogg", "duration": "1706000"}, {"album": "5 Symphonies | 7 Overtures", "creator": "Mendelssohn", "title": "symphony 5", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Mendelssohn/symphony 5.ogg", "duration": "1860000"}]}}}
"""
