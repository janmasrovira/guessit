module Collections.Tchaikovsky exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Tchaikovsky - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Manfred Symphony, The Voyevoda (Petrenko)", "creator": "Tchaikovsky", "title": "manfred symphony", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/manfred symphony.ogg", "duration": "3475000"}, {"album": "Symphonies No. 1 'Winter Dreams' & No. 2 'Little Russian'", "creator": "Tchaikovsky", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 1.ogg", "duration": "2725000"}, {"album": "Symphonies No. 1 'Winter Dreams' & No. 2 'Little Russian'", "creator": "Tchaikovsky", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 2.ogg", "duration": "1835000"}, {"album": "Symphony No. 3 'Polish' & Romeo & Juliet'", "creator": "Tchaikovsky", "title": "symphony 3", "trackNum": "2", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 3.ogg", "duration": "2073000"}, {"album": "Tchaikovsky Symphonies No.4-6 (Evgeny Mravinsky)", "creator": "Tchaikovsky", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 4.ogg", "duration": "2514000"}, {"album": "Tchaikovsky Sym Nr.4 & 5", "creator": "Tchaikovsky", "title": "symphony 5", "trackNum": "4", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 5.ogg", "duration": "2871000"}, {"album": "Tchaikovsky Sym Nr.6, Vaughan Williams Sym Nr.4", "creator": "Tchaikovsky", "title": "symphony 6", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Tchaikovsky/symphony 6.ogg", "duration": "2801000"}]}}}
"""
