module Collections.Schumann exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Schumann - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "NYPO / Schumann Symphonies 1 and 2", "creator": "Schumann", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Schumann/symphony 1.ogg", "duration": "2046000"}, {"album": "NYPO / Schumann Symphonies 1 and 2", "creator": "Schumann", "title": "symphony 2", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Schumann/symphony 2.ogg", "duration": "2418000"}, {"album": "CD48 R.Schumann Sym Nr.3-4", "creator": "Schumann", "title": "symphony 3", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Schumann/symphony 3.ogg", "duration": "1991000"}, {"album": "CD48 R.Schumann Sym Nr.3-4", "creator": "Schumann", "title": "symphony 4", "trackNum": "6", "location": "/media/jan/LocalDisk/Guessit/Schumann/symphony 4.ogg", "duration": "1813000"}]}}}
"""
