module Collections.Ravel exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Ravel - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Ravel: Bolero / Debussy: La Mer (Karajan)", "creator": "Ravel", "title": "Bolero", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Ravel/Bolero.ogg", "duration": "977000"}, {"album": "Bolero, Pavane pour une infante defunte, Concerto pour la main gauche, Rapsodie espagnole, La Valse (Immerseel)", "creator": "Ravel", "title": "La Valse", "trackNum": "8", "location": "/media/jan/LocalDisk/Guessit/Ravel/La Valse.ogg", "duration": "730000"}, {"album": "Boulez Conducts Ravel", "creator": "Ravel", "title": "Menuet Antique", "trackNum": "9", "location": "/media/jan/LocalDisk/Guessit/Ravel/Menuet Antique.ogg", "duration": "433000"}, {"album": "Daphnis et Chloe, Pavane, Bolero (Gergiev)", "creator": "Ravel", "title": "Pavane Pour une Infante Defunte", "trackNum": "11", "location": "/media/jan/LocalDisk/Guessit/Ravel/Pavane Pour une Infante Defunte.ogg", "duration": "420000"}, {"album": "Bolero, Pavane pour une infante defunte, Concerto pour la main gauche, Rapsodie espagnole, La Valse (Immerseel)", "creator": "Ravel", "title": "Rapsodie Espagnole", "trackNum": "4", "location": "/media/jan/LocalDisk/Guessit/Ravel/Rapsodie Espagnole.ogg", "duration": "943000"}]}}}
"""
