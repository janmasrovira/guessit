module Collections.Beethoven exposing (..)

import Collection exposing (..)

symphonic : Collection
symphonic = symphonies

symphonies : Collection
symphonies = {
    title="Beethoven - Symphonies", playlist=unsafeDecodePlaylist s}

s : String
s = """
{"playlist": {"trackList": {"track": [{"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 1", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 1.ogg", "duration": "1552000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 2", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 2.ogg", "duration": "2029000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 3", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 3.ogg", "duration": "2973000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 4", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 4.ogg", "duration": "1970000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 5", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 5.ogg", "duration": "2109000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 6", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 6.ogg", "duration": "2663000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 7", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 7.ogg", "duration": "2524000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 8", "trackNum": "6", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 8.ogg", "duration": "1552000"}, {"album": "Beethoven - Symphonies (Bernstein/NYPO)", "creator": "Beethoven", "title": "symphony 9", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Beethoven/symphony 9.ogg", "duration": "4217000"}]}}}
"""
