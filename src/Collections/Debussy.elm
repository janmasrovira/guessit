module Collections.Debussy exposing(..)

import Collection exposing (..)


symphonic : Collection
symphonic = {
    title="Debussy - Symphonic", playlist=unsafeDecodePlaylist s}


s : String
s = """
 {"playlist": {"trackList": {"track": [{"album": "Debussy: La Mer, Images, Etc. (Dutoit)", "creator": "Debussy", "title": "Jeux", "trackNum": "4", "location": "/media/jan/LocalDisk/Guessit/Debussy/Jeux.ogg", "duration": "1052000"}, {"album": "Debussy: La Mer, Images, Etc. (Dutoit)", "creator": "Debussy", "title": "La Mer", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Debussy/La Mer.ogg", "duration": "1388000"}, {"album": "Debussy: La Mer, Images, Etc. (Dutoit)", "creator": "Debussy", "title": "Le Martyre de St Sebastien", "trackNum": "5", "location": "/media/jan/LocalDisk/Guessit/Debussy/Le Martyre de St Sebastien.ogg", "duration": "1456000"}, {"album": "Debussy - Pelleas et Melisande Suite; etc (Abbado)", "creator": "Debussy", "title": "Nocturnes", "trackNum": "2", "location": "/media/jan/LocalDisk/Guessit/Debussy/Nocturnes.ogg", "duration": "1351000"}, {"album": "Debussy - Pelleas et Melisande Suite; etc (Abbado)", "creator": "Debussy", "title": "Prelude a l'apres-midi d'un faune", "trackNum": "1", "location": "/media/jan/LocalDisk/Guessit/Debussy/Prelude a l'apres-midi d'un faune.ogg", "duration": "567000"}]}}}
 """
