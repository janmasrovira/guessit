module HtmlAux exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Json.Encode
import Json.Decode
import Html.Events exposing (..)

currentTime : Int -> Attribute msg
currentTime s = stringProperty "currentTime" (toString s)

muted : Bool -> Attribute msg
muted s = boolProperty "muted" s

-- this is a terrible hack!
stop' : Bool -> Attribute msg
stop' s = if s then currentTime 9999999 else autoplay True

stringProperty : String -> String -> Attribute msg
stringProperty name string =
  property name (Json.Encode.string string)

boolProperty : String -> Bool -> Attribute msg
boolProperty name bool =
  property name (Json.Encode.bool bool)

intProperty : String -> Int -> Attribute msg
intProperty name int = stringProperty name (toString int)

role = stringProperty "role"

integrity = stringProperty "integrity"

crossorigin = stringProperty "crossorigin"

-- use onPlaying instead!!
onPlay : msg -> Attribute msg
onPlay msg = on "play" (Json.Decode.succeed msg)

onPlaying : msg -> Attribute msg
onPlaying msg = on "playing" (Json.Decode.succeed msg)

script : List (Attribute msg) -> List (Html msg) -> Html msg
script = node "script"

link : List (Attribute msg) -> List (Html msg) -> Html msg
link = node "link"
