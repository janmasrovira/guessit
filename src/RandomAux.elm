module RandomAux exposing (..)

import Random
import Platform.Cmd as Cmd
import Random.Extra exposing (constant)
import Random.Array
import Array
import Array exposing (Array)
import Debug

unsafeRandomElemA : Array a -> Random.Generator a
unsafeRandomElemA a =
    Random.map fromJust (Random.Array.sample a)

fromJust : Maybe a -> a
fromJust x =
    case x of
        Just a -> a
        _ -> Debug.crash "fromJust"

chooseK : List a -> Int -> Random.Generator (List a)
chooseK l k = chooseKA (Array.fromList l) k
            |> Random.map (Array.toList)

chooseKA : Array.Array a -> Int -> Random.Generator (Array.Array a)
chooseKA a k =
    case k of
        0 -> constant Array.empty
        k -> let gen = Random.Array.choose a
                 faux mx = case mx of
                    (Nothing, _) -> Debug.crash "ChooseKA: Array too small"
                    (Just x, tail) -> Random.map (\a -> Array.push x a) (chooseKA tail (k - 1))
             in gen `Random.andThen` faux
