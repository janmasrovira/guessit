module MaybeAux exposing (..)

catMaybes : List (Maybe a) -> List a
catMaybes l =
    case l of
        [] -> []
        (Nothing::l') -> catMaybes l'
        (Just x::l') -> x :: catMaybes l'
