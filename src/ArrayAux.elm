module ArrayAux exposing(..)

import Array exposing(Array)
import Array

concat : Array (Array a) -> Array a
concat a = Array.foldr Array.append Array.empty a

concatl : List (Array a) ->  Array a
concatl a = List.foldr Array.append Array.empty a

singleton : a -> Array a
singleton x = Array.fromList [x]
