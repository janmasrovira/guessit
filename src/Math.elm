module Math exposing (..)

-- x must be in [a, b]
intervalTrans : (Int, Int) -> (Int, Int) -> Int -> Int
intervalTrans (a, b) (c, d) x =
    let len1 = b - a
        len2 = d - c
        aux = toFloat (x*len2) / toFloat len1 + toFloat c
    in round aux

intervalTrans100 : (Int, Int) -> Int -> Int
intervalTrans100 ab = intervalTrans ab (0, 100)
