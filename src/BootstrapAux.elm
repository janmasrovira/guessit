module BootstrapAux exposing (..)

import String
import Html exposing (..)
import HtmlAux exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Bootstrap.Html exposing (..)
import Html.Shorthand exposing (..)
import MaybeAux exposing (..)
import Maybe
import String

type Style =
    Default
        | Info
        | Success
        | Primary
        | Warning
        | Danger

-- button size
type BSize =
    BDefault
    | Large
    | Small
    | XSmall

styleString : Style -> String
styleString s =
    case s of
        Default -> "default"
        Info -> "info"
        Success -> "success"
        Primary -> "primary"
        Warning -> "warning"
        Danger -> "danger"

bsizeString : BSize -> ClassString
bsizeString s =
    case s of
        BDefault -> ""
        Large -> "btn-lg"
        Small -> "btn-sm"
        XSmall -> "btn-xs"

-- important use https and not http
bootstrapPath : String
bootstrapPath = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"

css : String -> Html a
css path =
  link [ rel "stylesheet", href path
       -- , integrity integritySHA
       , crossorigin "anonymous"
       ] []

-- do not use integrity check, it does not work
integritySHA : String
integritySHA = "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"

divcf : List (Html msg) -> Html msg
divcf = div [class "container-fluid"]

cssBootstrap : Html msg
cssBootstrap = css bootstrapPath

jumbotron : String -> List (Attribute msg) -> List (Html msg) -> Html msg
jumbotron classStr a b = div (class (String.append "jumbotron " classStr)::a) b

bdisabled : Attribute msg
bdisabled = stringProperty "disabled" "disabled"

withBadge : (List (Attribute msg) -> List (Html msg) -> Html msg)
           -> Maybe String -> List (Attribute msg) -> List (Html msg) -> Html msg
withBadge cmp badge =
    case badge of
        Just badge' -> \ats cs -> cmp ats (cs ++ [text " ", span [class "badge"] [text (badge')]])
        Nothing -> cmp

mobileFirst : Html msg
mobileFirst =
    node "meta" [stringProperty "name" "viewport", stringProperty "content" "width=device-width, initial-scale=1" ] []

withBadges : (List (Attribute msg) -> List (Html msg) -> Html msg)
           -> List String -> List (Attribute msg) -> List (Html msg) -> Html msg
withBadges cmp badges =
    case badges of
        [] -> cmp
        bs -> \ats cs ->
              cmp ats
                  (cs ++ [text " "]
                       ++ (List.intersperse (text " ")
                               (List.map (\b -> span [class "badge"] [text b]) bs)))



btnB : Style -> BSize -> ClassString -> Bool -> List String -> BtnParam msg -> msg -> Html msg
btnB style bsize c enabled badges p =
    btnc ("btn-" ++ styleString style ++ " " ++ bsizeString bsize ++ " " ++ c) "button" enabled badges p

{-| Helper for creating buttons
-}
btnc : ClassString -> String -> Bool -> List String -> BtnParam msg -> msg -> Html msg
btnc c typ enabled badges {icon,label,tooltip} x =
  let filterJust = List.filterMap identity
  in withBadges button badges
      ( type' typ
        :: class' ("btn " ++ c)
        :: filterJust
            [ Maybe.map title tooltip
            , if enabled
              then Just (onClick x)
              else Nothing
            , if enabled
              then Nothing
              else Just bdisabled
            ]
      )
      ( case (icon, label) of
          (Just icon', Just label') -> [icon', text (' ' `String.cons` label')]
          (ic, lab) -> catMaybes [ic, Maybe.map text lab]
      )

ariaValueNow = intProperty "aria-valuenow"
ariaValueMin = intProperty "aria-valuemin"
ariaValueMax = intProperty "aria-valuemax"
style' = stringProperty "style"

-- now in [0,100]
progressBar : Style -> Bool -> Bool -> Int -> Html a
progressBar style animated striped now =
    div [class "progress"] [
         div [class
              (String.join " "
                   (catMaybes
                        [Just "progress-bar"
                        , Just ("progress-bar-" ++ styleString style)
                        , if striped
                          then Just "progress-bar-striped"
                          else Nothing
                        , if animated
                        then Just "active"
                        else Nothing]))
             , role "progressbar"
             , ariaValueNow now
             , ariaValueMin 0
             , ariaValueMax 100
             , style' ("width:" ++ toString now ++ "%")] []
        ]
