-- Welcome to the world of hardcoding!
-- Reason: Http.get does not work due to cross-origin access
module Collections exposing(..)

import Array
import ArrayAux
import Collection exposing (..)

import Collections.Albeniz as Albeniz
import Collections.Copland as Copland
import Collections.Debussy as Debussy
import Collections.Dvorak as Dvorak
import Collections.Beethoven as Beethoven
import Collections.Borodin as Borodin
import Collections.Brahms as Brahms
import Collections.Bruckner as Bruckner
import Collections.Mahler as Mahler
import Collections.Mendelssohn as Mendelssohn
import Collections.Prokofiev as Prokofiev
import Collections.Rachmaninoff as Rachmaninoff
import Collections.Ravel as Ravel
import Collections.RichardStrauss as RichardStrauss
import Collections.SaintSaens as SaintSaens
import Collections.Schubert as Schubert
import Collections.Schumann as Schumann
import Collections.Shostakovich as Shostakovich
import Collections.Sibelius as Sibelius
import Collections.Stravinsky as Stravinsky
import Collections.Tchaikovsky as Tchaikovsky
import Collections.TonyBanks as TonyBanks

allCollections : List Collection
-- allCollections = ArrayAux.concatl [ArrayAux.singleton symphonic, symphonics]
allCollections = (symphonic :: symphonics)

symphonic : Collection
symphonic = concatCollections "Symphonic" symphonics

symphonics : List Collection
symphonics = [ Albeniz.symphonic
             , Beethoven.symphonic
             , Borodin.symphonic
             , Brahms.symphonic
             , Bruckner.symphonic
             , Copland.symphonic
             , Debussy.symphonic
             , Dvorak.symphonic
             , Mahler.symphonic
             , Mendelssohn.symphonic
             , Prokofiev.symphonic
             , Rachmaninoff.symphonic
             , Ravel.symphonic
             , RichardStrauss.symphonic
             , SaintSaens.symphonic
             , Schubert.symphonic
             , Schumann.symphonic
             , Shostakovich.symphonic
             , Sibelius.symphonic
             , Stravinsky.symphonic
             , Tchaikovsky.symphonic
             -- , TonyBanks.symphonic
             ]
