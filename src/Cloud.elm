module Cloud exposing (cloudPath)

import Regex

bucketUrl : String
bucketUrl = "https://storage.googleapis.com/guessit-music/"


guessitPath : String
guessitPath = "/media/jan/LocalDisk/Guessit/"

cloudPath : String -> String
cloudPath loc = Regex.replace Regex.All (Regex.regex (Regex.escape guessitPath)) (\_ -> bucketUrl) loc
