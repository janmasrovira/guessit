module Collection exposing (..)

import Json.Decode as Json
import Json.Decode exposing ((:=))
import String
import Debug
import Task
import Http
import Result
import RandomAux
import Random
import Array
import Array exposing (Array)
import Random.Array
import Unsafe
import ArrayAux
import Html exposing (..)

type alias Track = {
        title : String
    , location : String
    , composer : String
    , duration : Int
    }

type alias Playlist = List Track

type alias Collection = {
        title : String
    , playlist : Playlist
    }

loadPlaylistByUrl : (Playlist -> msg) -> (Http.Error -> msg) -> String -> Cmd msg
loadPlaylistByUrl succeed failed url = Task.perform failed succeed (Http.get decodePlaylist url)

-- Returns a random track and a random start time.
randomTrackStart : Playlist -> Int -> Int -> Random.Generator (Playlist, Int, Int)
randomTrackStart pl cand dur =
    randomCandidates pl cand `Random.andThen` \c ->
        Random.int 0 (cand - 1) `Random.andThen` \t ->
            Random.map (\s -> (c, t, s)) (randomStart dur (Unsafe.getL "randomtrackstart" t c))

randomCandidates : Playlist -> Int -> Random.Generator Playlist
randomCandidates pl cand = RandomAux.chooseK pl cand

randomStart : Int -> Track -> Random.Generator Int
randomStart dur t = Random.int 0 (t.duration - dur)

unsafeStr2Int : String -> Int
unsafeStr2Int str = case String.toInt str of
                        Ok num -> num
                        Err err -> Debug.crash err


-- VERY IMPORTANT!!
-- Don't Trust Arrays immutability, they are bugged!!! Avoid changing this function
concatCollections : String -> List Collection -> Collection
concatCollections title cols =
    let lists = List.map (\c -> c.playlist) cols
    in {title=title, playlist=List.concat lists}

collectionSeconds : Collection -> Int
collectionSeconds c = c.playlist
                      |> List.map (.duration)
                      |> List.sum

collectionHours : Collection -> Int
collectionHours c = collectionSeconds c // 3600


decodeTrack : Json.Decoder Track
decodeTrack = Json.object4 Track
              ("title" := Json.string)
              ("location" := Json.string)
              ("creator" := Json.string)
              ("duration" := (Json.map (unsafeStr2Int >> (\x -> x//1000)) Json.string))

decodePlaylist : Json.Decoder Playlist
decodePlaylist = Json.at (["playlist", "trackList", "track"]) (Json.list decodeTrack)

unsafeDecodePlaylist : String -> Playlist
unsafeDecodePlaylist json =
    case Json.decodeString decodePlaylist json of
        Result.Ok p -> p
        _ -> Debug.crash "unsafeDecodePlaylist"

viewCollection : Collection -> Html msg
viewCollection col =
    let mkLine t = div [] [text (String.concat [t.composer, " ", t.title])]
    in div [] ([ h1 [] [text col.title] ]
                   ++
                   List.map mkLine col.playlist
              )
