module FontAwesome exposing (..)

import Html exposing (..)
import HtmlAux exposing (..)
import Html.Attributes exposing (..)

loadingSpinner : Html msg
loadingSpinner = p [class "fa fa-spinner fa-spin fa-3x fa-fw"] []

loadFA : Html msg
loadFA = link [rel "stylesheet", href "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"] []
