module Main exposing(..)

import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Http
import Collection exposing (Playlist, Track, Collection)
import Picker
import Collections
import HtmlAux exposing (..)
import Random
import Cloud
import Unsafe
import String
import Set exposing (Set)
import BootstrapAux exposing (..)
import Bootstrap.Html exposing (..)
import Time exposing (..)
import Math exposing (..)
import FontAwesome exposing (..)

version = "v0.5.9"

main : Program Never
main =
  App.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type Model = Ini IniState
           | Playing PlayingState
           | Error String

type alias IniState = {
        collection : Collection
    , options : Options
    }

type alias PlayingState = {
    collection : Collection
    , incorrects : Int
    , corrects : Int
    , options : Options
    , candidates : Playlist
    , trackIx : Int
    , start : Int
    , seconds : Int
    , playing : Bool
    , stop : Bool
    , tried : Set Int
    }

type alias GameOverState = {
        corrects : Int
    , options : Options
    , collection : Collection
    , killer : Track -- The track that made you lose
    }

type alias Options = {
        audioLength : Int
    , candidates : Int
    , maxLifes : Int
    }

type Msg =
    SetCollection Collection
        -- PlayTrack candidates trackIx start
        | PlayTrack Playlist Int Int
        | Correct
        | Incorrect Int
        | Pause
        | OnPlaying
        | Reset
        | Tick Time

defaultOptions : Options
defaultOptions = { audioLength=150
                 , candidates=5
                 , maxLifes=5
                 }

collectionPicker : Options -> Picker.Description Msg
collectionPicker opts =
    let mkEntry col = {
            text= col.title
        , event=SetCollection col
        , badges=[toString (List.length col.playlist) ++ " tracks"
                 , toString (Collection.collectionHours col) ++ " hours"]
        , enabled=True
        , style=Primary
        , bsize=BDefault
        }
        entries=List.map mkEntry (List.filter (isValidCollection opts) Collections.allCollections)
    in {entries=entries}

init : (Model, Cmd Msg)
init = (Ini {collection=Collections.symphonic, options=defaultOptions}, Cmd.none)

const : a -> b -> a
const x _ = x

id : a -> a
id x = x

isValidCollection : Options -> Collection -> Bool
isValidCollection opts col =
    List.all id [
         List.length col.playlist >= opts.candidates
        ]

playTrack : PlayingState -> Playlist -> Int -> Int -> (Model, Cmd Msg)
playTrack mod candidates track start =
    (Playing { mod |
                   trackIx=track
             , candidates=candidates
             , start=start
             , stop=False
             , seconds=0
             , playing=False
             , tried=Set.empty
        }, Cmd.none)

sendRandomTrack : Collection -> Options -> Cmd Msg
sendRandomTrack col opts =
    Random.generate
        (\ (cand, t, s) -> PlayTrack cand t s)
        (Collection.randomTrackStart col.playlist opts.candidates opts.audioLength)

startPlaying : IniState -> PlayingState
startPlaying is =
    { corrects = 0
    , incorrects = 0
    , collection = is.collection
    , start = 0
    , trackIx = 0
    , options = is.options
    , candidates = []
    , stop = False
    , seconds = 0
    , playing = False
    , tried = Set.empty
    }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case model of
        Ini is ->
            case msg of
                SetCollection col -> (Ini {is | collection = col}, sendRandomTrack col defaultOptions)
                PlayTrack cands trackIx start ->
                    playTrack (startPlaying is) cands trackIx start
                Tick _ -> (model, Cmd.none)
                _ -> Debug.crash "unexpected msg Ini"
        Playing ps ->
            case msg of
                PlayTrack cands trackIx start ->
                    playTrack ps cands trackIx start
                Correct ->
                    let state' = addCorrect ps
                    in (Playing state'
                       , sendRandomTrack state'.collection state'.options)
                Incorrect ix ->
                    let state' = addIncorrect ix ps
                    in (Playing state', Cmd.none)
                Pause ->
                    let state' = stop ps
                    in (Playing state', Cmd.none)
                Tick t ->
                    let model' = if ps.playing
                               then (addSecond >> mbStop >> Playing) ps
                               else model
                    in (model', Cmd.none)
                OnPlaying ->
                    (Playing {ps | playing = True}, Cmd.none)
                Reset ->
                    init
                _ -> Debug.crash "unexpected msg"
        Error er -> init

addIncorrect : Int -> PlayingState -> PlayingState
addIncorrect ix state = {state | incorrects = state.incorrects + 1
                     , tried = Set.insert ix state.tried}

addCorrect : PlayingState -> PlayingState
addCorrect state = {state | corrects = state.corrects + 1}

stop : PlayingState -> PlayingState
stop state = {state | stop = True }

addSecond : PlayingState -> PlayingState
addSecond state = {state | seconds = state.seconds + 1}

-- stops the track if the time is over
mbStop : PlayingState -> PlayingState
mbStop state =
    if state.options.audioLength < state.seconds
    then {state | stop = True}
    else state

-- VIEW
resetButton : Html Msg
resetButton = btnDanger_ {icon=Nothing
                           , label=Just "Reset"
                           , tooltip=Just "Go back to collection selection"} Reset

view : Model -> Html Msg
view model =
    case model of
        Ini is ->
            divcf ([
                 cssBootstrap
                , mobileFirst
                , jumbotron "text-center" [] [
                      h1 [] [text "Guessit"]
                     , p [] [text "janmasrovira@gmail.com"]
                     , p [] [text version]
                     , div [class "hidden-lg hidden-md"]--[class "visible-xs-inline", class "visible-sm-inline", class "visible-md-inline"]
                          [h2 [class "text-danger"] [text ""]
                          , div [class "alert alert-danger", role "alert"] [h3 [] [text "It does not work on mobile devices"]]]
                     ] ] ++ [Picker.view (collectionPicker is.options)])
        Playing ps ->
            let track = Unsafe.getL (String.concat ["view ", toString ps.trackIx, " ", toString (List.length ps.candidates)] ) ps.trackIx ps.candidates
                mkEntry ix t =
                    let event =
                            if ps.trackIx == ix then Correct
                            else Incorrect ix
                        triedIx = Set.member ix ps.tried
                    in {event=event
                       , text=String.concat [t.composer, " - ", t.title]
                       , badges=[]
                       , enabled=not triedIx
                       , style=Primary
                       , bsize=BDefault
                       }
                candPicker = {entries=List.indexedMap mkEntry ps.candidates}
            in divcf
                ([ loadFA
                 , mobileFirst
                 , audio [src (Cloud.cloudPath track.location), controls False
                         , autoplay True, currentTime ps.start, stop' ps.stop
                         , onPlaying OnPlaying] [ ]
                 , cssBootstrap
                 , jumbotron "text-center" [] [h1 [] [text "Guessit"]
                                              , h3 [] [text "janmasrovira@gmail.com"]
                                              ]
                 , if ps.playing
                   then progressBar Success False False
                       (intervalTrans100 (0, ps.options.audioLength) ps.seconds)
                   else p [class "text-center"] [loadingSpinner]
                 -- else progressBar Warning True True 100
                 , Picker.view candPicker
                 , h3 [style [("color", "red")]] [text (toString ps.incorrects)]
                 , h3 [style [("color", "green")]] [text (toString ps.corrects)]
                 , resetButton
                ])
        Error err ->
            div [] [ h1 [] [text "Error!"]
                   , h2 [  ] [ text err ]
                   , resetButton
                   ]

-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions model =
  Time.every (Time.second) Tick

showHttpError : Http.Error -> String
showHttpError error =
    case error of
        Http.Timeout -> "timeout"
        Http.BadResponse _ _ -> "badresponse"
        Http.UnexpectedPayload m -> "unexpected payload:" ++ m
        Http.NetworkError -> "NetworkError"
