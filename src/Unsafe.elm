module Unsafe exposing (..)

import Array
import String

fromJust : String -> Maybe a -> a
fromJust ctx m =
    case m of
        Just x -> x
        _ -> Debug.crash (String.append "fromJust: " ctx)

get : String -> Int -> Array.Array a -> a
get ctx ix a = Array.get ix a
             |> fromJust (String.append "Array.get: " ctx)

getL : String -> Int -> List a -> a
getL s i l =
    case List.drop i l of
        [] -> Debug.crash ("getL: " ++ s)
        (a::_) -> a
